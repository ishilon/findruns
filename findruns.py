# This is the Python version of the original H.E.S.S. perl script. 
# It follows the same "philosophy" as the original script.
#   
# findruns : locate runs within a radius of a test position
# this is a nicer version of the original, with better help and option handling
#
# Requires the radec2gal and gal2radec programs in summary/bin for
# --galactic mode to work correctly
# 

import os # For environment variables
import math #usage of PI and other mathematical functions
import MySQLdb as maria
import sys
from optparse import OptionParser # Parser for command line options (GetOpt::Long -> GetOptions() function in perl )
import ConfigParser # To read .dbtoolsrc
import subprocess

PI 			  	= math.pi
end_date  		= '2020-01-01'
hess_latitude 	= -23.27166
outfile 	  	= None
HOME 		  	= os.environ["HOME"]
HESSROOT 	  	= os.environ["HESSROOT"]
goodname 		= None

gal2radec_path = HESSROOT + "/summary/bin/gal2radec"
radec2gal_path = HESSROOT + "/summary/bin/radec2gal"

#Read the config file required to conenct to the DB
def Read_DB_Config( section ) : 
	path = HOME + "/.dbtoolsrc"
	if os.path.isfile( path ) :
		config = ConfigParser.ConfigParser()
		config.read( path )	
		dbuser = config.get( section, 'user' )
		dic = {}
		dic[ 'port' ] 	= config.getint( section, 'port' )
		dic[ 'host' ] 	= config.get( section, 'host' )
		dic[ 'user' ] 	= config.get( section, 'user' )
		dic[ 'passwd' ]	= config.get( section, 'password' )
		dic[ 'db' ] 	= config.get( section, 'database' )
		return ( dic )
	else :
		print("Error: .dbtoolsrc not found in the specified ($HOME) directory")
    		sys.exit(1)

#DB connection function
def DB_connect( dsn, dic ) :
	try:
		conn = maria.connect( **dsn )
		if cl_options.verbose :
			print "Connected to DB server"
	except maria.Error, e:
		print "Cannot connect to server ( Error code:", e.args[0],")" 
		print 'Error Message: ', e.args[1]
		sys.exit( 1 )
	else:
		if dic :
			cursor = conn.cursor(maria.cursors.DictCursor)
		else :
			cursor = conn.cursor()
		return conn, cursor

def get_list_of_run_selection_types( dsn ) :
	sql = "SELECT DISTINCT SelectionType FROM Run_Selection_Criteria_Set"
	dsn[ 'db' ] = 'HD_MonitorParam'
	conn, cursor = DB_connect( dsn, False )
	cursor.execute( sql )
	if cl_options.verbose :
		print "Fetching available selection types"
	tmp = cursor.fetchall()
	types = list(  )
	for item in tmp :
		types.append( item[0] )

	conn.close()
	if cl_options.verbose :
		print "Disconnected from DB server"
	return types

def lookup_radec_from_sourcename( sourcename ):
	print 'Looking up the source ' + sourcename +'...\n'
	astro_sel_path = HESSROOT + '/dbtools/util/astro_select'
	if not os.path.exists( astro_sel_path ) :
		print ("*" * 80 + "\n" +
               "The program 'astro_select' was not found in your $HESSROOT\n"
               "directory. This program is needed to look up the RA/DEC coordinates\n"
               "of '%s'. It's possible that you need to compile it... \n"
               "Change to the $HESSROOT/dbtools directory and type: 'make programs; make install' .\n"
               "Then, try running findruns again.\n" %( sourcename ) +
               "*" * 80)
		sys.exit(1)

	sources = subprocess.check_output( [astro_sel_path, '--degrees', sourcename] ) # Gives: Ignored 1 line(s) that could not be read properly.

	if len( sources ) == 0 :
		print 'No source found in catalog matching: %s' %( sourcename )
		sys.exit(1)
	else:
		sources_list = sources.split( '\n' )[ :-1 ]
		if len( sources_list ) == 1 or cl_options.pickfirst : 
			source_selected = sources_list[0]
			name, ra, dec, temp, other = source_selected.split( '\t' )[:5]
			other_tmp = other.strip( '()' )
			if cl_options.verbose :
				print 'Your source is the %s, %s, (%s, %s).' %( other_tmp, name, ra, dec )
		else:
			print 'The following sources matched the source name %s:\n' %( sourcename )
			while True :
				choice_list = []
				for i in range( 0, len( sources_list ) ) :
					print 'source %i: %s' %( i+1, sources_list[i] )
					choice_list.append( str(i + 1) )
				choice = raw_input( '\nPlease choose a source: ' )
				if choice in choice_list : 
					source_selected = sources_list[ int(choice) - 1 ]
					break
				else : 
					print 'Please correct your choice. \n'
					continue
			name, ra, dec, temp, other = source_selected.split( '\t' )[:5]
			other_tmp = other.strip( '()' )
			print 'You chose the %s, %s, (%s, %s) as your source. \n' %( other_tmp, name, ra, dec )
					
		nicename = name.replace( ' ', '' )

		return float(ra), float(dec), nicename

def coordinates_trans( path, alpha, beta ) :
	bufsize = 0
	pipe = subprocess.Popen( path, bufsize = bufsize, 
			stdin = subprocess.PIPE, stdout = subprocess.PIPE )
	coordinates = str( alpha ) + ' ' + str( beta )
	stdout_data, stderr_data = pipe.communicate( coordinates )
	if pipe.returncode != 0 :
		raise RuntimeError("%r failed, status code %s stdout %r stderr %r" % (
			path, pipe.returncode, stdout_data, stderr_data))
	str_a, str_b = ( stdout_data.split('\n')[:-1] )[0].split(' ')[:2]
	#pipe.terminate()
	return float( str_a ), float( str_b )

def is_inside_box( x, y ):
	if galactic :
		x, y = coordinates_trans( radec2gal_path, x, y )
		if x < 180.0 :
			x += 360.0
		if cl_options.boxminx < 180.0 : 
			cl_options.boxminx += 360.0
		if cl_options.boxmaxx < 180.0 :
			cl_options.boxmaxx += 360.0
	
	if cl_options.boxminx <= x and x <= cl_options.boxmaxx :
		if cl_options.boxminy <= y and y <= cl_options.boxmaxy :
			return True
	return False

# returns approximate factor for given psi value 
# (distance from obs to test point)
def acceptance( d ) :
	psi_sqr = d * d
	sig = 4.0
	return math.exp( -1 * pow( psi_sqr, 2)/2.0/pow( sig, 2) )

#Written by Lars Mohrmann
def check_telpattern( pattern ):
    if ( pattern%2 != 0 or pattern < 0 or pattern > 62 ):
        raise ValueError( 'Invalid telpattern: %s'%( pattern ) )

#Written by Lars Mohrmann
def count_tels_in_telpattern( pattern ):
    check_telpattern( pattern )
    ntel = 0
    for i in range( 1, 6 ):
        if ( pattern & ( 0x1 << i ) ):
            ntel += 1
    return ntel

#Written by Lars Mohrmann
def check_telpattern( pattern ):
    if ( pattern%2 != 0 or pattern < 0 or pattern > 62 ):
        raise ValueError( 'Invalid telpattern: %s'%( pattern ) )
def telpattern_to_string(pattern):
	check_telpattern(pattern)
	string = ''
	for i in range(1, 6):
		if ( ( pattern >> i ) & 0x1 ):
			string += str(i)
		else:
			string += '_'
	return string

def check_runs_and_zenith( runs, dsn ) :
	runstr = ''
	for run in runs : 
		runstr = runstr + run + ','
	runstr = runstr[:-1]

	if cl_options.ecap :
		sql = 	'SELECT * FROM Monitor_Run_Selection WHERE Entry IN (SELECT MAX(Entry) ' \
 				'FROM Monitor_Run_Selection WHERE  Run in ({0}) ' \
 				"AND SelectionType='{1}' AND WhenEntered<'{2}' GROUP BY Run)".format( runstr, cl_options.quality_selection_type, cl_options.quality_date ) 
	else :
		sql = 	'SELECT *FROM (SELECT * FROM Monitor_Run_Selection ' \
				"WHERE SelectionType='{0}' " \
        		"AND Run in ({1}) AND WhenEntered<'{2}' ORDER BY WhenEntered DESC) " \
       			'AS Runs GROUP BY Run'.format( cl_options.quality_selection_type, runstr, cl_options.quality_date )
	conn, cursor = DB_connect( dsn, True )
	cursor.execute( sql )
	tmp = cursor.fetchall()
	goodruns = []
	for row in tmp :
		runs_dict[ str( row['Run'] ) ]['Pattern'] = row['GoodTels']
		runs_dict[ str( row['Run'] ) ]['Ntel'] = count_tels_in_telpattern( row['GoodTels'] )
		runs_dict[ str( row['Run'] ) ]['Notes'] = row['Notes']
		if runs_dict[ str( row['Run'] ) ]['Ntel'] >= cl_options.mintels :
			goodruns.append( str( row['Run'] ) )

	runstr_zenith = ''
	for run in goodruns : 
		runstr_zenith = runstr_zenith + run + ','
	runstr_zenith = runstr_zenith[:-1]

	goodzenithruns = []
	if cl_options.ecap :
		sql_zenith = 	'SELECT * FROM Monitor_Run_Tracking WHERE Entry IN (SELECT MAX(Entry) ' \
   						'FROM Monitor_Run_Tracking WHERE Run in ({0}) ' \
   						"AND WhenEntered<'{1}' GROUP BY Run)".format( runstr_zenith, cl_options.quality_date )
	else :
		sql_zenith = 	'SELECT *FROM (' \
        				'SELECT * FROM Monitor_Run_Tracking ' \
        				"WHERE Run in ({0}) AND WhenEntered<'{1}' ORDER BY WhenEntered DESC) " \
       					'AS Runs GROUP BY Run'.format( runstr_zenith, cl_options.quality_date )

	conn, cursor = DB_connect( dsn, True )
	cursor.execute( sql_zenith )
	tmp_zenith = cursor.fetchall()
	for row in tmp_zenith :
		zenith = 90.0 - row['Alt_mean']
		if zenith > cl_options.minzenith and zenith < cl_options.maxzenith :
			goodzenithruns.append( str( row['Run'] ) )

	conn.close()
	if cl_options.verbose :
		print "Disconnected from DB server"
	return sorted( goodzenithruns )


def check_runs_new_db( runs, dsn ) :
	sql = None
	runstr = ''
	for run in runs : 
		runstr = runstr + run + ','
	runstr = runstr[:-1]

	if cl_options.ecap :
		sql = 	'SELECT * FROM Monitor_Run_Selection WHERE Entry IN (SELECT MAX(Entry) ' \
 				'FROM Monitor_Run_Selection WHERE  Run in ({0}) ' \
 				"AND SelectionType='{1}' AND WhenEntered<'{2}' GROUP BY Run)".format( runstr, cl_options.quality_selection_type, cl_options.quality_date ) 
	else :
		sql = 	'SELECT *FROM (SELECT * FROM Monitor_Run_Selection ' \
				"WHERE SelectionType='{0}' " \
        		"AND Run in ({1}) AND WhenEntered<'{2}' ORDER BY WhenEntered DESC) " \
       			'AS Runs GROUP BY Run'.format( cl_options.quality_selection_type, runstr, cl_options.quality_date )
	conn, cursor = DB_connect( dsn, True )
	cursor.execute( sql )
	tmp = cursor.fetchall()
	goodruns = []
	for row in tmp :
		runs_dict[ str( row['Run'] ) ]['Pattern'] = row['GoodTels']
		runs_dict[ str( row['Run'] ) ]['Ntel'] = count_tels_in_telpattern( row['GoodTels'] )
		runs_dict[ str( row['Run'] ) ]['Notes'] = row['Notes']
		if runs_dict[ str( row['Run'] ) ]['Ntel'] >= cl_options.mintels :
			goodruns.append( str( row['Run'] ) )

	conn.close()
	if cl_options.verbose :
		print "Disconnected from DB server"
	return sorted( goodruns )

def check_zenith_angle( runs, dsn ) :
	runstr = ''
	for run in runs : 
		runstr = runstr + run + ','
	runstr = runstr[:-1]
	goodzenithruns = []
	if cl_options.ecap :
		sql = 	'SELECT * FROM Monitor_Run_Tracking WHERE Entry IN (SELECT MAX(Entry) ' \
   				'FROM Monitor_Run_Tracking WHERE Run in ({0}) ' \
   				"AND WhenEntered<'{1}' GROUP BY Run)".format( runstr, cl_options.quality_date )
	else :
		sql = 	'SELECT *FROM (' \
        		'SELECT * FROM Monitor_Run_Tracking ' \
        		"WHERE Run in ({0}) AND WhenEntered<'{1}' ORDER BY WhenEntered DESC) " \
       			'AS Runs GROUP BY Run'.format( runstr, cl_options.quality_date )
	conn, cursor = DB_connect( dsn, True )
	cursor.execute( sql )
	tmp = cursor.fetchall()
	for row in tmp :
		zenith = 90.0 - row['Alt_mean']
		if zenith > cl_options.minzenith and zenith < cl_options.maxzenith :
			goodzenithruns.append( str( row['Run'] ) )

	conn.close()
	if cl_options.verbose :
		print "Disconnected from DB server"
	return sorted( goodzenithruns )

def write_runlist( outfile, runs ) :
	file = open( outfile, 'w' )
	for run in runs :
		if not cl_options.nocomments :
			note = runs_dict[run]['Notes']
			if note :
				if len( note ) <= 4 :
					note = None
		patstr = telpattern_to_string( runs_dict[run]['Pattern'] )

		if note :
			file.write( '{0} {1} [{2}] {3} {4} {5} {6} {7} {8} {9} {10} \t #{11}\n'.format( run, runs_dict[run]['Pattern'], 
						patstr, runs_dict[run]['Hemisphere'], runs_dict[run]['Target'], runs_dict[run]['RA'], runs_dict[run]['DEC'], 
						runs_dict[run]['Distance'], runs_dict[run]['Duration'], runs_dict[run]['Exposure'], runs_dict[run]['Ntel'], note ) )
		else :
			file.write( '{0} {1} [{2}] {3} {4} {5} {6} {7} {8} {9} {10}\n'.format( run, runs_dict[run]['Pattern'], patstr, 
						runs_dict[run]['Hemisphere'], runs_dict[run]['Target'], runs_dict[run]['RA'], runs_dict[run]['DEC'], 
						runs_dict[run]['Distance'], runs_dict[run]['Duration'], runs_dict[run]['Exposure'], runs_dict[run]['Ntel'] ) )
	file.close()

def round2( strnum ) :
	if type( strnum ) is str : 
		return str( round( float( strnum ), 2 ) )
	elif type( strnum ) is float :
		return str( round( strnum, 2 ) )
	else :
		print 'Type is neither str nor float. Use a different method'

def write_summary( runs ) :
	total_duration = 0.0
	total_exposure = 0.0
	avg_offs = 0.0
	disp_offs = 0.0
	total_runs = 0
	i = 0

	print '\n'
	print '-' * 90
	print ( 'Run' + ' ' * 4 + 'Dist' + ' ' * 3 + 'Tel' + ' ' * 3 + 'Qual' + ' ' * 6 + 'Target' 
			+ ' ' * 19 + 'Hem' + ' ' * 4 + 'Date' + ' ' * 7 + 'Dur' + ' ' * 4 + 'Exp' ) #+ ' ' * 5 + 'Comments' ) 
	print '-' * 90


	for run in runs :
		if runs_dict[run]['Pattern'] or True : #in findruns.pl or !$check -> $check is not defined anywhere in the file

			if i == 50 :
				i = 0
				print '\n'
				print '-' * 90
				print ( 'Run' + ' ' * 4 + 'Dist' + ' ' * 3 + 'Tel' + ' ' * 3 + 'Qual' + ' ' * 6 + 'Target' 
						+ ' ' * 19 + 'Hem' + ' ' * 4 + 'Date' + ' ' * 7 + 'Dur' + ' ' * 4 + 'Exp' ) #+ ' ' * 5 + 'Comments' ) 
				print '-' * 90

			total_duration 	+= float( runs_dict[run]['Duration'] )
			total_exposure 	+= float( runs_dict[run]['Exposure'] )
			avg_offs 		+= float( runs_dict[run]['Distance'] )
			disp_offs		+= pow( float(runs_dict[run]['Distance']), 2 )
			total_runs 		+= 1
			i += 1

			print '%-6s %-7s %-3s [%-5s]    %-25s %-3s %-10s  %-6s  %-8s'%( run, round2( runs_dict[run]['Distance'] ), str( runs_dict[run]['Ntel'] ), 
					telpattern_to_string( runs_dict[run]['Pattern'] ), runs_dict[run]['Target'], runs_dict[run]['Hemisphere'],
					runs_dict[run]['Date'], round2( float( runs_dict[run]['Duration'] )/60.0 ), round2( runs_dict[run]['Exposure'] ) )

	print '-' * 90

	if total_runs == 0 :
		print 'No runs were found for your search criteria'
		return
	disp_offs /= total_runs
	avg_offs /= total_runs
	offs_err = math.sqrt( abs( disp_offs - pow( avg_offs,2 ) ) )

	print '\n'
	print '-' * 75
	print '\t TOTAL DURATION: {0} hours (for {1} runs)'.format( round( total_duration/3600.0, 2 ), total_runs )
	print '\t AVG RUN OFFSET: {0} +/- {1} deg'.format( round( avg_offs, 2 ), round( offs_err, 2 ) )
	print '\t CORRECTED EXPOSURE: {0} hours (normalized to {1} deg offset)'.format( round( total_exposure/3600.0, 2 ), cl_options.expoffs )
	print '\t LIST WRITTEN TO: {0}'.format( outfile )
	print '-' * 75

def show_run_selection_criteria( dsn ):

	dsn['db'] = 'HD_MonitorParam'
	if cl_options.ecap :
		sql = 	'SELECT *  FROM Run_Selection_Criteria_Set ' \
				"WHERE (SelectionType='{0}') ORDER BY SetNum DESC LIMIT 1".format( cl_options.quality_selection_type )
	else :
		sql = 	'SELECT *  FROM Run_Selection_Criteria_Set ' \
				"WHERE (SelectionType='{0}') ORDER BY SetNum DESC LIMIT 1".format( cl_options.quality_selection_type )

	conn, cursor = DB_connect( dsn, True )
	try :
		cursor.execute( sql )
		row = cursor.fetchone()
	except maria.Error, e:
		print "Could not retrieve result ( Error code:", e.args[0],")" 
		print 'Error Message: ', e.args[1] 
		return 
	if not row :
		print 'Could not retrieve result - no selection type was specified'
		return
	setnum = row['SetNum']

	if cl_options.ecap :
		sql = 	'SELECT * FROM Run_Selection_Criteria WHERE SetNum={0}'.format( setnum )
	else :
		sql = 	'SELECT * FROM Run_Selection_Criteria WHERE SetNum={0}'.format( setnum )
	
	print '\n'
	print ' ' * 46 + "Run selection criteria for '{0}'".format( cl_options.quality_selection_type ) + ' ' * 47
	print '-'*125
	print 'Criterion' + ' ' * 32 + 'Cut Range' + ' ' * 20 + 'Cuts' + ' ' * 6 + 'Comment'  
	print '-'*125

	cursor.execute( sql )
	res = cursor.fetchall()
	for item in res :
		subsystem 	= item['Subsystem']
		criterion 	= item['Criterion']
		cuts 		= item['Cuts']

		if ( criterion == 'DST' ) :
			explanation = 'DST has to exist'
			cutrange = ' ' * 12 + '---'
		else :
			if cl_options.ecap :
				sql = 	'SELECT * FROM Monitor_Quality_Set ' \
						"WHERE (SubSystem='{0}') ORDER BY SetNum DESC LIMIT 1".format( subsystem )
			else :
				sql = 	'SELECT * FROM Monitor_Quality_Set ' \
				"WHERE (SubSystem='{0}') ORDER BY SetNum DESC LIMIT 1".format( subsystem )

			cursor.execute( sql )
			row2 = cursor.fetchone()
			setnum2 = row2['SetNum']
			
			if cl_options.ecap :
				sql = 	'SELECT * FROM Monitor_Quality ' \
						"WHERE SetNum={0} AND ColumnName='{1}'".format( setnum2, criterion )
			else :
				sql = 	'SELECT * FROM Monitor_Quality ' \
						"WHERE SetNum={0} AND ColumnName='{1}'".format( setnum2, criterion )

			cursor.execute( sql )
			res2 = cursor.fetchone()
			explanation = res2['Title']
			if criterion == 'Duration' :
				explanation += ' (s)'
			if cuts == 'strict' :
				high = res2['GoodMax']
				low = res2['GoodMin']
			elif cuts == 'loose' :
				high = res2['RangeMax']
				low = res2['RangeMin']
			else :
				print "The set of cuts neither 'strict' nor 'loose'!"
			cutrange = "[ %10s , %10s ]"%("%.5f"%(low), "%.5f"%(high))
		print "%-41s%-29s%-10s%s"%("%s.%s"%(subsystem, criterion), cutrange, cuts, explanation)
	print '-'*125
	conn.close()
	if cl_options.verbose :
		print "\nDisconnected from DB server\n"


############################################################
#Get Command Line Options
############################################################

parser = OptionParser()

parser.add_option( "--first", action = "store", type = "int", dest = "firstrun", 
		default = 10000, help = "Start looking after the specified run number (default is 10000)" )
parser.add_option( "--last", action = "store", type = "int", dest = "lastrun", 
		default = -1, help = "skip runs after specified run number (default is -1, for all runs)" )

parser.add_option( "--mintels", action = "store", type = "int", dest = "mintels", 
		default = 0, help = "Require runs to have at least the given number of telescopes passing data quality selection." )

parser.add_option( "--name", action = "store", type = "string", dest = "sourcename", 
		help = 	"Specify a source name (or regular expression pattern) instead of " \
				'RA/Dec position. The position will be looked up in the HESS'
				'databases. Be sure to enclose the name in quotes to avoid problems'
				'(especially if the name includes spaces). The % character can be used'
				'as a wildcard. If more than one source matches your pattern, you will'
				'be asked to choose a specific match. You need the correct "astro" section in'
				'your ~.dbtoolsrc file for this to work.' )

parser.add_option( "--selection", action = "store", type = "string", dest = "quality_selection_type", 
		help = 	"Perform run quality selection on the runs using the WebSummary database tables." 
		 		"The TYPE options should be a run selection type (typically 'detection' or 'spectral' " \
		 		", however a list of possible run selections will be printed if the one you choose is not found.)" )

parser.add_option( "--expoffs", action = "store", type = "float", dest = "expoffs", 
		default = 0.7, help = "Set the observation offset to which the exposure correction should be " \
				'normalized (in degrees). The default is 0.7 degrees, which is the'
				'typical wobble offset.' ) # Relative offset to use for exposure calculation

parser.add_option( "--dbport", action = "store", type = "int", dest = "dbport", 
		default = 0, help = 'If you are using SSH port forwarding from outside the MPIK ' \
				"(web: www.mpi-hd.mpg.de/hfm/HESS/intern/HD-LinuxCluster/db.shtml), " \
				"then you can define your local machine and the forwarding port " \
		        "with the options --dbhost and --dbport. You may also use " \
		        "--dbconfig to specify these options in your ~/.dbtoolrc instead." )

parser.add_option( "--dbhost", action = "store", type = "string", dest = "dbhost", 
		help = "DB host name (see DB port help for more details)" )

parser.add_option( "--dbname", action = "store", type = "string", dest = "dbname", 
		default = 'HD_Monitor', help = "DB name" )

parser.add_option( "--dbuser", action = "store", type = "string", dest = "dbuser", 
		help = "DB user-name" )

parser.add_option( "--dbpasswd", action = "store", type = "string", dest = "dbpasswd", 
		help = "DB password for user-name" )

parser.add_option( "--output", action = "store", type = "string", dest = "useroutput", 
		help = 	"Write the runlist to the specified filename instead of the" \
				'automatically generated one (see OUTPUT below).' \
				'This option still modifies the output filename a bit ...' \
				'if you want to simply specify the filename yourself use' \
				'the --outputfile option instead.' )

parser.add_option( "--outputfile", action = "store", type = "string", dest = "outputfile", 
		help = 	"Write the runlist to the given output filename. " \
				'Useful for scripting.' \
				'Overrides the --output option.' )

parser.add_option( "--dbconfig", action = "store", type = "string", dest = "dbconfig", 
		default = "hess", help = "Specify the name of the database configuration in ~/.dbtoolsrc to use." )
 
parser.add_option( "--quality-before-date", action = "store", type = "string", dest = "quality_date", default = end_date,
		help = 	"search old entries in the run-selection database (e.g. query the state" \
				'of the run selection before the specified date, for historical searches).'
				'Date should be in the format: 2008-01-28.' )

parser.add_option( "--start-date", action = "store", type = "string", dest = "startdate", 
		default = "2001-01-01", help = "Only choose runs after the given date" )

parser.add_option( "--end-date", action = "store", type = "string", dest = "enddate", 
		default = "2020-01-01", help = "Only choose runs before the given date" )

parser.add_option( "--min-zenith", action = "store", type = "float", dest = "minzenith", 
		default = 0.0, help = "Minimum value of the mean zenith angle for a given run" )

parser.add_option( "--max-zenith", action = "store", type = "float", dest = "maxzenith", 
		default = 90.0, help = "Maximal value of the mean zenith angle for a given run" )

parser.add_option( "--ra", "--Ra", "--RA", action = "store", type = "float", dest = "ra", 
		default = -1.0, help = "RA" )

parser.add_option( "--dec", "--Dec", "--DEC", action = "store", type = "float", dest = "dec", 
		default = -1.0, help = "DEC" )

parser.add_option( "--gall", action = "store", type = "float", dest = "gal_l", 
		default = -1.0, help = "Galactic longitude" )

parser.add_option( "--galb", action = "store", type = "float", dest = "gal_b", 
		default = -1.0, help = "Galactic latitude" )

parser.add_option( "--Rmin", "--rmin", action = "store", type = "float", dest = "rmin", 
		default = 0.0, help = "Specify minimal offset Radius" )

parser.add_option( "--Rmax", "--rmax", "--R", "-r", action = "store", type = "float", dest = "rmax", 
		default = 360.0, help = "Specify maximal offset Radius" )

parser.add_option( "--boxminx", "--minx", action = "store", type = "float", dest = "boxminx", 
		default = -1.0, help = "Box min X" )

parser.add_option( "--boxmaxx", "--maxx", action = "store", type = "float", dest = "boxmaxx", 
		default = -1.0, help = "Box max X" )

parser.add_option( "--boxminy", "--miny", action = "store", type = "float", dest = "boxminy", 
		default = -1.0, help = "Box min Y" )

parser.add_option( "--boxmaxy", "--maxy", action = "store", type = "float", dest = "boxmaxy", 
		default = -1.0, help = "Box max Y" )

parser.add_option( "--modelpprunlist", action = "store", type = "string", dest = "modelpprunlist", 
		help = "1 column runlist from Model++ that will be checked in the HD Run Selection criteria (detection or spectral). " \
				"The output will be a HAP-like runlist including HD criteria results" )

#  Flag options
parser.add_option( "--ecap", action = "store_true", dest = "ecap", 
		default = True, help = "Are you running on the ECAP MariaDB server?" )
parser.add_option( "--notinecap", action = "store_false", dest = "ecap", 
		help = "Are you running on the ECAP MariaDB server?" )

parser.add_option( "-q", "--quiet", action = "store_false", dest = "verbose", 
		default = False, help = "Don't print status messages to stdout" )
parser.add_option( "-v", "--verbose", action = "store_true", dest = "verbose", 
		help = "Print status messages to stdout" )

parser.add_option( "--hess", action = "store_true", dest = "hess", 
		default = False, help = "Use HESS_tables database instead of default HD_tables" \
								' (the HESS_tables database reflects the state of runs in Namibia, and not calibrated ' \
								"runs)" )

parser.add_option( "--count", action = "store_true", dest = "count", 
		default = False, help = "Just return the run count instead of printing the run summary. This is " \
								"useful for batch scripts to check for new runs. No runlist is written." )

parser.add_option( "--galactic", action = "store_true", dest = "galactic", 
		default = False, help = "Interpret the position given in the command line in Galactic coordinates" \
								", instead of RA/Dec. Also, notice that to enable this option you must provide" \
								"Galactic coordinates as the command line options (--gall --galb)" )

parser.add_option( "--box", action = "store_true", dest = "box", 
		default = False, help = "Search for runs in a box instead of a radius from a test point, useful " \
								"for collecting scan runs for example. Note that if this option is" \
								'enabled, the exposure will not be calculated correctly (since there is' \
								'no test position). This should work for galactic scan boxes, but may' \
								"have problems around RA=0 if you are not in --galactic mode." \
								'Also notice that for this option to be enabled, the box dimensions should' \
								'should be given as the command line options (--boxmaxx --boxmaxy --boxminx --boxminy).')

parser.add_option( "--allruns", action = "store_true", dest = "allruns", 
		default = False, help = "Obtain a list of all runs (in this case no RA/Dec and radius input is required)" )

parser.add_option( "--testdb", action = "store_true", dest = "testdb", 
		default = False, help = "Use HD_test database instead of default HD_tables (for testing purposes)" )

parser.add_option( "--no-comments", action = "store_true", dest = "nocomments", 
		default = False, help = "disable run-wise output of DQ comments on the console as well as in the runlist. " )

parser.add_option( "--pickfirst", action = "store_true", dest = "pickfirst", 
		default = False, help = "Choose the first source matching the --name pattern if there are " \
								"multiple matches (for automation)" )

#parser.add_option( "--help", action = "store_true", dest = "help", 
#		default = False, help = " " )

parser.add_option( "--man", action = "store_true", dest = "man", 
		default = False, help = "display the man page, with more information and examples" )

( cl_options, args ) = parser.parse_args()

not_coor = ( ( cl_options.ra == -1.0 or cl_options.dec == -1.0 ) and ( cl_options.gal_l == -1.0 or cl_options.gal_b == -1.0 ) )
#print not_coor_and_radius

not_srcName = not cl_options.sourcename
#print not_srcName_and_radius
not_defined_box = cl_options.boxminx == -1.0 or cl_options.boxmaxx == -1.0 or cl_options.boxminy == -1.0 or cl_options.boxmaxy == -1.0
#print not_defined_box

not_init_cond = not_coor and not_srcName and not_defined_box
#print not_init_cond and not cl_options.allruns and not cl_options.modelpprunlist

galactic 	= cl_options.gal_l >= 0.0 and cl_options.gal_b >= 0.0
box 		= cl_options.boxminx >= 0.0 and cl_options.boxmaxx >= 0.0 and cl_options.boxminy >= 0.0 and cl_options.boxmaxy >= 0.0

if ( not_init_cond and not cl_options.allruns and not cl_options.modelpprunlist ) or cl_options.man :
	if ( not_init_cond and not cl_options.allruns and not cl_options.modelpprunlist ) :
		print "Please specify either:  \n1) Ra, Dec (or gal_l and gal_b) and radius: ( --ra, --dec, -r ) or ( --gall, --galb, -r ) \n" \
										"2) Ra, Dec (or gal_l and gal_b) + Rmin and Rmax: ( --ra, --dec, --rmin, --rmax ) or ( --gall, --galb, --rmin, --rmax ) \n" \
		 								"3) Sourcename and radius/radii \n" \
		 								"4) Box dimensions \n" \
		 								"5) The --allruns option \n" \
		 								"6) The --modelpprunlist option" 
	parser.print_help()
	sys.exit(1)

print ( '=' * 60 + '\nPreparing your runs list\n' + '=' * 60 )

dsn = { "db" 	: cl_options.dbname,
 		"host" 	: cl_options.dbhost,
 		"port" 	: cl_options.dbport,
 		"user" 	: cl_options.dbuser,
 		"passwd": cl_options.dbpasswd }

if cl_options.testdb : 
	dsn['db'] = 'HD_test'

if not cl_options.dbhost : # get info from the .dbtoolsrc
	dsn = Read_DB_Config( cl_options.dbconfig )

seltypes = get_list_of_run_selection_types( dsn )

dsn[ 'db' ] = cl_options.dbname

#dsn = { 	"host" : cl_options.dbhost,
#		"port" : cl_options.dbport,
#		"db"   : dsn.dbname }

if cl_options.quality_selection_type :
	if cl_options.quality_selection_type not in seltypes :
		print ( '=' * 70 )
		print "Available run selection types: (specify with --selection <type>)"
		for selection in seltypes :
			print "	*", selection
		sys.exit(1)

#======================================================================
# setup 
#======================================================================

if cl_options.modelpprunlist :
	print "Extracting HD data quality criteria for a Model++ runlist..."
	if not cl_options.useroutput :
		outfile = cl_options.modelpprunlist + '.lis'
	else : 
		outfile = cl_options.useroutput
	if cl_options.outputfile :
		outfile = cl_options.outputfile

	bufsize = 0
	check_runlist = HOME + '/summary/scripts/check_runlist.pl'
	cl_args = "--selection '{0}' --quality-before-date='{1}' --only-run-numbers --runlist '{2}' --outputfile './$outfile'".format( cl_options.quality_selection_type, cl_options.quality_date, cl_options.modelpprunlist, outfile )
	args = [check_runlist, cl_args ]
	pipe = subprocess.Popen( args, bufsize = bufsize, 
			stdin = subprocess.PIPE, stdout = subprocess.PIPE )
	stdout_data, stderr_data = pipe.communicate( )
	print stdout_data

if cl_options.lastrun > 0 :
	if cl_options.firstrun >= cl_options.lastrun :
		print '=' * 80
		print 'You first and last runs values do not make sense.\n Please check your arguments...'
		sys.exit(1)

#Check if needed
if cl_options.allruns :
	cl_options.ra 		= 0.
	cl_options.dec 		= 0.

else :
	if not box :
		if cl_options.sourcename :
			galactic = False		# don't want galactic mode in this case
			cl_options.ra, cl_options.dec, goodname = lookup_radec_from_sourcename( cl_options.sourcename )
		else :
			if cl_options.verbose and cl_options.ra >= 0 and cl_options.dec >= 0 :
				print "Using ({0}, {1}) as Ra/Dec coordinates".format( cl_options.ra, cl_options.dec)
			elif cl_options.verbose and cl_options.gal_l >= 0 and cl_options.gal_b >= 0 :
				print "Using ({0}, {1}) as Galactic coordinates".format( cl_options.gal_l, cl_options.gal_b)

		if ( cl_options.rmin >= cl_options.rmax ) :
			print ( "=" * 80 + "\n" + "Your set Rmin >= Rmax. \n"
					'Please check your command-line arguments.\n'
					"=" * 80 )
			sys.exit(1)

		if ( cl_options.rmin > 1e10 or cl_options.rmin < 0 ) :
			print 	( "=" * 80 + "\n" +
					 "You specified a minimal radius of: {0}.\nThis value doesn't make sense.".format( cl_options.rmin ) + 
					 " Please check your command-line arguments. \n" +
					 "=" * 80 )
			sys.exit(1)

		if ( cl_options.rmax > 1e10 or cl_options.rmax < 1e-10 ) :
			print 	( "=" * 80 + "\n" +
					 "You specified a maximal radius of: {0}.\nThis value doesn't make sense.".format( cl_options.rmax ) + 
					 " Please check your command-line arguments. \n" +
					 "=" * 80 )
			sys.exit(1)

		if ( cl_options.minzenith >= cl_options.maxzenith ) :
			print ( "=" * 80 + "\n" +
					"Minimal zenith angle is greater than or equals to maximal zenith angle. \n" 
					"Check your command-line arguments. \n"
					"=" * 80 )
	    		sys.exit(1)

		if galactic :
			if cl_options.verbose :
				print 'Galactic coordinates mode: opening pipes to coordinate conversion programs'
			cl_options.ra, cl_options.dec = coordinates_trans( gal2radec_path, cl_options.gal_l, cl_options.gal_b )
			if ( cl_options.verbose ) :
				print 'Succesfuly transformed Galactic coordinates to Ra/Dec'

	else :
		if ( cl_options.boxminx < 0 or cl_options.boxmaxx < 0 or cl_options.boxminy < 0 or cl_options.boxmaxy < 0
				or cl_options.boxminx >= cl_options.boxmaxx or cl_options.boxminy >= cl_options.boxmaxy ) :
			print ( "Your box' dimensions do not make sense. \n"
					'Please check your command-line arguments.' )
			sys.exit(1)

#build dbdump command on the Monitor_Run_Data table
dbdump = HESSROOT + '/dbutils/bin/dbdump'
table = 'Monitor_Run_Data' 
if cl_options.hess :
	table = 'HESS_tables.' + table
table = ''.join( ['-t ', table] )

query = ''.join( [ '-c Run >= ', str( cl_options.firstrun ) ] )

if cl_options.lastrun > 0 :
	query = ''.join( [ query, ' AND Run <= ', str( cl_options.lastrun ) ] )

query = ''.join( [ query, ' AND (Run_Start_Time>\'', str( cl_options.startdate ), '\' AND Run_Start_Time<\'', str( cl_options.enddate ), '\')' ] )

if cl_options.mintels > 0 :
	query = ''.join( [ query, ' AND (Number_Of_Telescopes>=', str( cl_options.mintels ), ')' ] )

query = ''.join( [ query, " AND Events > 1000 AND RunType like 'Observation%'" ] )

if galactic :
	outfile = 'gal-'
else :
	outfile = 'radec-'

if box :
	outfile += 'box-' + str( cl_options.boxminx ) + '_' + str( cl_options.boxmaxx ) + '_' + str( cl_options.boxminy ) + '_' + str( cl_options.boxmaxy )
elif galactic :
	outfile += str( cl_options.gal_l ) + '_' + str( cl_options.gal_b ) + '_' + str( cl_options.rmin ) + '_' + str( cl_options.rmax )
else :
	outfile += str( cl_options.ra ) + '_' + str( cl_options.dec ) + '_' + str( cl_options.rmin ) + '_' + str( cl_options.rmax )

if cl_options.sourcename and goodname :
	outfile = goodname + '_' + str( cl_options.rmin ) + '_' + str( cl_options.rmax )

if cl_options.allruns :
	outfile = 'all-runs'

if cl_options.useroutput :
	outfile = str( cl_options.useroutput )

#======================================================================
# Gather run information:
#======================================================================
bufsize = 0
#print dbdump + table + query
args = [dbdump, table, query ]
pipe = subprocess.Popen( args, bufsize = bufsize, 
		stdin = subprocess.PIPE, stdout = subprocess.PIPE )
stdout_data, stderr_data = pipe.communicate( )
if len( stdout_data ) == 0 :
	print ( 'No runs passing your run selection criteria were found. \n'
			'Try changing your first and/or last runs, or mintel value' )
	sys.exit(1)
else :
	runs = stdout_data.split('\n')

runs_dict = {}

# Clean all the the junk generated by dbdump!!!!
for source_str in runs : 
	#print source_str
	if source_str == None or source_str == ' ' or source_str == '' or source_str == 'QueryDB end' or source_str == 'here' or source_str == 'QueryDB' or '\t' not in source_str :
		continue
	elif 'tracking data' in source_str :
		if cl_options.verbose :
			print 'Tracking data entry ommited'
		continue

	( run_num, run_type, target, off_mode, off_x, off_y, ra, dec, ntel, tels, 
	dsets, start, first_ev, duration, events, space ) = source_str.split( '\t' )

	if run_num == 0 :
		continue 	#skip malformed entry with blank run
	run_type_lowcase = run_type.lower()
	if 'muon' in run_type_lowcase :
		continue 	# skip muon runs
	if not target :
		target = 'unknown'
	elif ' ' in target :
		target = target.replace( ' ', '_')
	
	cosdec = math.cos( cl_options.dec * ( PI/180.0 ) )
	
	if off_x < -180.0 :
		off_x += 360.0
	
	d_ra = cosdec * ( cl_options.ra - ( float( ra ) + float( off_x ) ) )
	
	if d_ra < -180.0 :
		d_ra += 360.0 	# resolves a problem with RA=0 transition
	if d_ra > 180.0 :
		d_ra = 360.0 - d_ra 	# resolves a problem with RA=0 transition

	d_dec = ( cl_options.dec - ( float( dec ) + float( off_y ) ) )

	dist = math.sqrt( pow( d_ra, 2 ) + pow( d_dec, 2 ) )

	if box :
		in_range = is_inside_box( float( ra ) + float( off_x ), float( dec ) + float( off_y ) )
		dist = 0.0 	# no distance for box
	else :
		in_range = cl_options.rmin <= dist and cl_options.rmax >= dist

	if in_range or cl_options.allruns :
		# build database of runs...
		exposure = ( acceptance( dist ) / acceptance( cl_options.expoffs ) ) * float( duration )
		if float( dec ) > hess_latitude :
			hemisphere = 'N'
		else :
			hemisphere = 'S'

		date, time = start.split( ' ' )

		corra = float(ra) + float(off_x)
		cordec = float(dec) + float(off_y)

		source_dict = { 'Run_Type' : run_type, 'Target' : target, "Off_Mode" : off_mode,
				'RA' : str( float(ra) + float(off_x) ), 'DEC' : str( float(dec) + float(off_y) ), 'Ntel' : ntel, 'Tels' : tels, 'Dsets' : dsets, 
				'Date' : date, 'First_ev' : first_ev, 'Duration' : duration, 'Events' : events,
				'Exposure' : exposure, 'Distance' : dist, 'Hemisphere' : hemisphere, 'Pattern' : 0, 'Notes' : ' ' }
		runs_dict[run_num] = source_dict

if ( len( runs_dict ) == 0 or cl_options.count ) and not cl_options.allruns :
	print ( 'No runs were found that match your search criteria. \n'
			'Please check your command-line arguments - or try another source.' )
	sys.exit(1)

runs = sorted( runs_dict )
goodzenithruns = []

if cl_options.quality_selection_type :
	
	number_of_runs = len ( runs_dict )
	#should unite check_runs_new_db and check_zenith_angle to a single function (done)
	#goodruns = check_runs_new_db( runs, dsn )
	#goodzenithruns = check_zenith_angle( goodruns, dsn )
	goodzenithruns = check_runs_and_zenith( runs, dsn )
	runs = goodzenithruns
	
	if cl_options.outputfile :
		outfile = cl_options.outputfile
	else :
		outfile = '{0}_{1}.lis'.format( outfile, cl_options.quality_selection_type )
	if not cl_options.count :
		write_runlist( outfile, runs )
else :
	if cl_options.outputfile :
		outfile = cl_options.outputfile
	else :
		outfile = '{0}-unchecked.lis'.format( outfile )
	write_runlist( outfile, runs )

if not cl_options.count :
	# normally, print out a nice formatted list:
	write_summary( runs )
else :
	# if count is specified, just return the number of runs found
	# (useful for batch checking for new runs)
	print len( runs_dict ), '\n'

if not cl_options.nocomments :
	show_run_selection_criteria( dsn )
